# ChatSecure-iOS-Precompiled-Dependencies
Do not use these in your app! This is only to speed up automated Travis-CI builds.

### CPAProxy Dependencies

* **[Tor][tor]**: 0.2.9.8
* **[libevent][libevent]**: 2.0.22-stable
* **[OpenSSL][openssl]**: 1.0.2j

[tor]: https://www.torproject.org/
[libevent]: http://libevent.org/
[openssl]: https://www.openssl.org/

### OTRKit Dependencies

* **[libgpg-error](https://www.gnupg.org/(de)/related_software/libgpg-error/index.html)**: 1.26
* **[libgcrypt](http://www.gnu.org/software/libgcrypt/)**: 1.7.5
* **[libotr](https://otr.cypherpunks.ca)**: 4.1.1

### Pods

4.0 (58) dafd61fec29b77ead8d8c3462a5aee2a8eb632c2